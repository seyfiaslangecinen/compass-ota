package com;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.util.List;

@Component
@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    private CustomEndpointInterceptor customEndpointInterceptor;

    public WebServiceConfig(CustomEndpointInterceptor customEndpointInterceptor){
        this.customEndpointInterceptor = customEndpointInterceptor;
    }

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

    @Bean(name = "request_OTA_HotelAvailRQ")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema otaSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("request_OTA_HotelAvailRQPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://www.opentravel.org/OTA/2003/05");
        wsdl11Definition.setSchema(otaSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema otaSchema() {
        return new SimpleXsdSchema(new ClassPathResource("request_OTA_HotelAvailRQ.xsd"));
    }

    @Override
    public void addInterceptors(List<EndpointInterceptor> interceptors) {

        // register global interceptor
        interceptors.add(customEndpointInterceptor);
    }
}
