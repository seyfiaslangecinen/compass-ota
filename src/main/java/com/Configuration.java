package com;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Configuration {

    private String kindName;

    public Configuration(
            @Value("${kind_name}") String kindName){
        this.kindName = kindName;
    }

    public String getKindName() {
        return kindName;
    }
}
