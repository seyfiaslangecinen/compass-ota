package com.Datastore;

public class IntegrationData {

    private String name;
    private String consumerKey;

    public static final String NAME = "name";

    public static final String CONSUMER_KEY = "consumerKey";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConsumerKey() {
        return consumerKey;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }
}
