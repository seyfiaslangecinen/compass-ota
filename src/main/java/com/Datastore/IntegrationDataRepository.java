package com.Datastore;

import com.Configuration;
import com.google.cloud.datastore.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegrationDataRepository {

    private Configuration configuration;
    private Datastore datastore;
    private KeyFactory keyFactory;

    @Autowired
    public IntegrationDataRepository(Configuration configuration){
        datastore = DatastoreOptions.getDefaultInstance().getService();
        keyFactory = datastore.newKeyFactory()
                .setKind(configuration.getKindName())
                .setNamespace("ChannelManagerIntegration");
        this.configuration = configuration;

    }

    public IntegrationData entityToUser(Entity entity){
        IntegrationData integrationData = new IntegrationData();
        integrationData.setName(entity.getKey().getName());
        integrationData.setConsumerKey(entity.getString(IntegrationData.CONSUMER_KEY));
        return integrationData;
    }

    public IntegrationData findUser(String name){
        Key key = keyFactory.newKey(name);
        Entity userEntity = datastore.get(key);
        return entityToUser(userEntity);
    }
}
