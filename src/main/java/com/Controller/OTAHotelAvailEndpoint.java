package com.Controller;

import com.Service.OTAHotelAvailService;
import org.opentravel.ota._2003._05.OTAHotelAvailRQ;
import org.opentravel.ota._2003._05.OTAHotelAvailRS;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class OTAHotelAvailEndpoint {

    public OTAHotelAvailService otaHotelAvailService;

    public OTAHotelAvailEndpoint(OTAHotelAvailService otaHotelAvailService){
        this.otaHotelAvailService = otaHotelAvailService;
    }

    public static final String NAMESPACE_URI = "http://www.opentravel.org/OTA/2003/05";

    public static final String LOCAL_PART = "OTA_HotelAvailRQ";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "OTA_HotelAvailRQ")
    @ResponsePayload
    public OTAHotelAvailRS otaHotelAvailRS(@RequestPayload OTAHotelAvailRQ otaHotelAvailRQ){

        return otaHotelAvailService.otaHotelAvailServiceResponse(otaHotelAvailRQ);
    }
}
