package com.Service;

import org.opentravel.ota._2003._05.OTAHotelAvailRQ;
import org.opentravel.ota._2003._05.OTAHotelAvailRS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OTAHotelAvailService {

    private UserProvider userProvider;

    @Autowired
    public  OTAHotelAvailService(UserProvider userProvider){
        this.userProvider = userProvider;
    }

    public OTAHotelAvailRS otaHotelAvailServiceResponse(OTAHotelAvailRQ otaHotelAvailRQ){


            OTAHotelAvailRS otaHotelAvailRS = new OTAHotelAvailRS();

            OTAHotelAvailRS.RoomStays roomStays = new OTAHotelAvailRS.RoomStays();
            OTAHotelAvailRS.RoomStays.RoomStay roomStay = new OTAHotelAvailRS.RoomStays.RoomStay();
            OTAHotelAvailRS.RoomStays.RoomStay.RoomTypes roomTypes = new OTAHotelAvailRS.RoomStays.RoomStay.RoomTypes();
            OTAHotelAvailRS.RoomStays.RoomStay.RoomTypes.RoomType roomType = new OTAHotelAvailRS.RoomStays.RoomStay.RoomTypes.RoomType();
            OTAHotelAvailRS.RoomStays.RoomStay.RoomTypes.RoomType.RoomDescription roomDescription =
                    new OTAHotelAvailRS.RoomStays.RoomStay.RoomTypes.RoomType.RoomDescription();

            roomDescription.setName("Standard Single Room");
            roomType.setRoomDescription(roomDescription);

            OTAHotelAvailRS.RoomStays.RoomStay.RatePlans ratePlans = new OTAHotelAvailRS.RoomStays.RoomStay.RatePlans();
            OTAHotelAvailRS.RoomStays.RoomStay.RatePlans.RatePlan ratePlan =
                    new OTAHotelAvailRS.RoomStays.RoomStay.RatePlans.RatePlan();

            OTAHotelAvailRS.RoomStays.RoomStay.RatePlans.RatePlan.RatePlanDescription ratePlanDescription =
                    new OTAHotelAvailRS.RoomStays.RoomStay.RatePlans.RatePlan.RatePlanDescription();
            ratePlanDescription.setName("BB net refundable");

            roomTypes.setRoomType(roomType);
            roomStay.setRoomTypes(roomTypes);

            ratePlan.setRatePlanDescription(ratePlanDescription);
            ratePlans.setRatePlan(ratePlan);
            roomStay.setRatePlans(ratePlans);
            roomStays.addRoomStay(roomStay);

            otaHotelAvailRS.setRoomStays(roomStays);

            return otaHotelAvailRS;
    }
}
