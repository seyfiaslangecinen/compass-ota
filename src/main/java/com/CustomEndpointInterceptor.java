package com;

import com.Datastore.IntegrationDataRepository;
import com.Datastore.IntegrationData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.Service.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;

import javax.naming.AuthenticationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;

@Component
public class CustomEndpointInterceptor implements EndpointInterceptor {

    private static final Log LOG = LogFactory.getLog(CustomEndpointInterceptor.class);

    public IntegrationDataRepository datastoreDao;

    public UserProvider userProvider;

    @Autowired
    public CustomEndpointInterceptor (IntegrationDataRepository datastoreDao, UserProvider userProvider){
        this.datastoreDao = datastoreDao;
        this.userProvider = userProvider;
    }

    @Override
    public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
        LOG.info("Endpoint Request Handling");

        WebServiceMessage webServiceMessageRequest = messageContext.getRequest();
        SoapMessage soapMessage = (SoapMessage) webServiceMessageRequest;
        SoapHeader soapHeader = soapMessage.getSoapHeader();
        Source bodySource = soapHeader .getSource();
        DOMSource bodyDomSource = (DOMSource) bodySource;
        org.w3c.dom.Node bodyNode = bodyDomSource.getNode();

       String username = bodyNode.getFirstChild().getNextSibling().getFirstChild().
                getNextSibling().getFirstChild().getNextSibling().getFirstChild().getNodeValue();

        String password = bodyNode.getFirstChild().getNextSibling().getFirstChild().
                getNextSibling().getFirstChild().getNextSibling().
                getNextSibling().getNextSibling().getFirstChild().getNodeValue();

        IntegrationData integrationData = datastoreDao.findUser(username);

        if (!integrationData.getConsumerKey().equals(password)) {
            throw new AuthenticationException("Invalid Authentication");
        }else {
            userProvider.setUsername(username);
            userProvider.setPassword(password);
        }
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
        LOG.info("Endpoint Response Handling");
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
        LOG.info("Endpoint Exception Handling");
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {
        LOG.info("Execute code after completion");
    }
}
